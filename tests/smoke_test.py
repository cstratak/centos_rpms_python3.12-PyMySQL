#!/usr/bin/python3
# Modified from
# https://github.com/PyMySQL/PyMySQL/blob/v1.0.2/example.py

import pymysql
connection = pymysql.connect(unix_socket="/var/lib/mysql/mysql.sock", db="mysql")
cursor = connection.cursor()
cursor.execute("SELECT Host,User FROM user")
print(cursor.description)

cursor.close()
connection.close()
